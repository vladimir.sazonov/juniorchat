import { getCurrentInstance } from '@vue/composition-api'
import ChangeLanguageInQuasar from './changeLanguageInQuasar'

const InitialSettings = (): void => {
  const ctx = getCurrentInstance()
  // Устанавливаем шаблонные размеры ширины экрана
  ctx.$q.screen.setSizes({ sm: 480, md: 769, lg: 993, xl: 1200 })

  // Проверяем сайт открыт на мобильной платформе или нет
  ctx.$store.dispatch('main/isMobile', !!ctx.$q.platform.is.mobile)

  // Разные браузеры отдают локализацию в разных форматах
  const listLanguages = {
    en: 'en-gb',
    'en-us': 'en-gb',
    'en-US': 'en-gb',
    ru: 'ru-ru',
    'ru-ru': 'ru-ru'
  }

  if (ctx.$q.cookies.get('localization')) {
    ctx.$i18n.locale = listLanguages[ctx.$q.cookies.get('localization').localization] || 'en-gb'
  } else {
    ctx.$i18n.locale = listLanguages[ctx.$q.lang.getLocale()] || 'en-gb'
  }

  ChangeLanguageInQuasar(ctx)
}

export default InitialSettings
