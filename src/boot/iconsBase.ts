import IconBase from '../components/icons/IconBase.vue'
import { boot } from 'quasar/wrappers'

export default boot(({ Vue }) => {
  Vue.component('IconBase', IconBase)

  Vue.config.productionTip = false
})
