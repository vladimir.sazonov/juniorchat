import { ActionTree } from 'vuex'
import { StoreInterface } from '../index'
import { ExampleStateInterface } from './mainState'

const actions: ActionTree<ExampleStateInterface, StoreInterface> = {
  isMobile ({ commit }, value) {
    commit('isMobile', value)
  },

  user ({ commit }, value) {
    commit('user', value)
  },

  chat ({ commit }, value) {
    commit('chat', value)
  },

  isoLanguage ({ commit }, value) {
    commit('isoLanguage', value)
  }
}

export default actions
