import { RouteConfig } from 'vue-router'
import { IsUser } from 'pages/auth/Auth.vue'
import state from '../store/main/mainState'

const routes: RouteConfig[] = [
  {
    path: '/',
    component: () => import('layouts/MainLayout.vue'),
    children: [
      { path: '', component: () => import('pages/auth/Auth.vue') },
      {
        path: '/chat',
        component: () => import('pages/chat/Chat.vue'),
        beforeEnter: (to, from, next) => {
          !IsUser(state.user) ? next() : next('/')
        }
      }
    ]
  }
]

if (process.env.MODE !== 'ssr') {
  routes.push({
    path: '*',
    component: () => import('pages/Error404.vue')
  })
}

export default routes
