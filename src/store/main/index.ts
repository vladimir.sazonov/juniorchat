import { Module } from 'vuex'
import { StoreInterface } from '../index'
import state, { ExampleStateInterface } from './mainState'
import actions from './mainActions'
import getters from './mainGetters'
import mutations from './mainMutations'

const module: Module<ExampleStateInterface, StoreInterface> = {
  namespaced: true,
  actions,
  getters,
  mutations,
  state
}

export default module
