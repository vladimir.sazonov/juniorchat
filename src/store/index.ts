import { store } from 'quasar/wrappers'
import Vuex from 'vuex'
import main from './main'

export interface StoreInterface {
  example: unknown;
}

export default store(function ({ Vue }) {
  Vue.use(Vuex)

  const Store = new Vuex.Store<StoreInterface>({
    modules: {
      main
    },

    strict: !!process.env.DEV
  })

  return Store
})
