// import { getCurrentInstance } from '@vue/composition-api'
// import { VueConstructor } from 'vue'

const ChangeLanguageInQuasar = (ctx): void => {
  // const ctx: InstanceType<VueConstructor> | null = getCurrentInstance()

  // Формат локализации для Quasar
  const listLanguages = {
    'en-gb': 'en-gb',
    'ru-ru': 'ru'
  }

  // Меняем язык в компонентах Quasar
  import(`quasar/lang/${listLanguages[ctx.$i18n.locale]}`).then(lang => {
    ctx.$q.lang.set(lang.default)
  })
}

export default ChangeLanguageInQuasar
