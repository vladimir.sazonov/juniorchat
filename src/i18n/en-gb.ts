export default {
  nameChat: 'Junior chat',
  enterYourNickname: 'Enter your nickname',
  enter: 'Enter',
  uploadAvatar: 'Upload an avatar'
}
