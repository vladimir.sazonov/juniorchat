export interface ExampleStateInterface {
  isMobile: boolean;
  user: object;
  users: object;
  chat: string[];
  isoLanguage: string;
}

const state: ExampleStateInterface = {
  isMobile: false,
  user: {},
  users: [
    {
      nickname: 'Timo Cook',
      avatar: 'TimCook.jpg',
      id: 472375643652839
    }
  ],
  chat: [],
  isoLanguage: 'en-gb'
}

export default state
