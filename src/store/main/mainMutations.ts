import { MutationTree } from 'vuex'
import { ExampleStateInterface } from './mainState'

const mutation: MutationTree<ExampleStateInterface> = {
  isMobile (state: ExampleStateInterface, value) {
    state.isMobile = value
  },

  user (state: ExampleStateInterface, value) {
    state.user = value
  },

  chat (state: ExampleStateInterface, value) {
    state.chat.push(value)
  },

  isoLanguage (state: ExampleStateInterface, value) {
    state.isoLanguage = value
  },
}

export default mutation
