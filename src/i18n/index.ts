import enGB from './en-gb'
import ruRU from './ru-ru'

export default {
  'en-gb': enGB,
  'ru-ru': ruRU
}
